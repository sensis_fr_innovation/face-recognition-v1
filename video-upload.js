// References to HTML
const PRESENTATION_VIDEOS_FOLDER = 'presentations/'
const API_BASE_URL = "http://localhost:8001/api"
let videoJsPlayer = null

/*
    Onload calls
*/
getAvailableVideosFromServer()

$('#master-button').css('display', 'none')

/*
    Event Listeners
*/
$('#file-upload-form-submit').click(function (e) {
    e.preventDefault()
    postUploadFileToServer()
})

$('#btn-refresh').click(function () {
    getAvailableVideosFromServer()
})

$('#master-button').click(function () {
    videoJsPlayer = videojs('vd-presentation')

    videoJsPlayer.requestFullscreen()
    videoJsPlayer.enterFullWindow()

    // before fully playing, the facial recognition module must be ready first
    videoJsPlayer.play()
    

    // start the whole application here (face detection and all)
    startFaceApiDetection()
})


/*
    API functions
*/
function getAvailableVideosFromServer() {
    const xhttp = new XMLHttpRequest()
    xhttp.open('GET', API_BASE_URL + '/presentations', true)
    xhttp.onload = function() {
        createVideoListOnUi(JSON.parse(this.response))
    }
    xhttp.send()
}

function postUploadFileToServer() { 
    const form = document.getElementById('file-upload-form')
    const formData = new FormData(form)

    const xhttp = new XMLHttpRequest()
    xhttp.open('POST', API_BASE_URL + '/file-upload', true)
    xhttp.onload = function () {
        // Refreshes the UI with the newly uploaded file
        getAvailableVideosFromServer()
    }
    xhttp.send(formData)
}

/* 
    UI funcitons
*/
function createVideoListOnUi(videoList) {
    clearVideoList()

    videoList.forEach((item, index) => {
        let listItem = $('<li></li>')
        let anchorItem = document.createElement('a')

        anchorItem.innerHTML = item
        anchorItem.addEventListener('click', function(e) {
            setPresentationToVideoPlayer(item)
            $('#master-button').css('display', 'block')
        })
        listItem.append(anchorItem)
        $('#available-videos').append(listItem)
    })
}

function clearVideoList() {
    const availableVideos = document.getElementById('available-videos')
    while (availableVideos.lastChild) {
        availableVideos.lastChild.remove()
    }
}

function setPresentationToVideoPlayer(filename) {
    // the program will expect that the file has been uploaded to the presentation folder already
    const videoJsPlayer = videojs('vd-presentation')
    videoJsPlayer.src([
        {type: 'video/mp4', src: PRESENTATION_VIDEOS_FOLDER + filename}
    ])
}