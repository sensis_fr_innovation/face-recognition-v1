const video = document.getElementById('video');
const resultButton = document.getElementById("generateDataTable");

Promise.all([
    faceapi.nets.tinyFaceDetector.loadFromUri('/models'),
    faceapi.nets.faceLandmark68Net.loadFromUri('/models'),
    faceapi.nets.faceRecognitionNet.loadFromUri('/models'),
    faceapi.nets.faceExpressionNet.loadFromUri('/models'),
]).then(startVideo)

function startVideo() {
    resultButton.disabled = true;
    navigator.getUserMedia(
        { video: {} },
        stream => video.srcObject = stream,
        err => console.error(err)
    )
}

function stopFaceDetection(stream) {
    stream.getTracks().forEach(track => {
      track.stop();
    });
}

const getSumOfNumbers = numbers => {
    const sum = numbers.reduce((a, b) => {
        return a + b
    }, 0)
    return sum
}

const getVideoName = (path = "") => {
	const chunks = (path && path.split("/")) || [""]
    const fileName = chunks[chunks.length - 1]
    const [videoName] = (fileName && fileName.split(".")) || ["UNKOWN"]
    return videoName
}

let logCounter = 0
const expressionLabels = ["neutral", "happy", "sad", "angry", "fearful", "disgusted", "surprised"]
let faceDetectionStopped = false
const finalResult = {};
const dataContainer = [];
const cont = []

const finalComputation = function() {

    for (let i in finalResult) {
        if (finalResult.hasOwnProperty(i)) {
            cont[i] = (cont[i] * 100)
        }
    }
    return cont;
}

const getResultConvertedToHundredths = (results) => {
    let holder = []
    results.forEach(result => {
        let obj = {}
        for (let property in result) {
            if (result.hasOwnProperty(property)) {
                if (property !== "time") {
                    obj[property] = (result[property] * 100)
                } else {
                    obj[property] = result[property]
                }
                
            }
        }
        holder.push(obj)
    })
    return holder
}

const dataResultTable = function(){


    document.getElementById('userNeutral').innerHTML = (finalResult.neutral * 100).toFixed(2);
    document.getElementById('userHappy').innerHTML = (finalResult.happy * 100).toFixed(2);
    document.getElementById('userSad').innerHTML = (finalResult.sad * 100).toFixed(2);
    document.getElementById('userAngry').innerHTML = (finalResult.angry * 100).toFixed(2); 
    document.getElementById('userFearful').innerHTML = (finalResult.fearful * 100).toFixed(2); 
    document.getElementById('userDisgusted').innerHTML = (finalResult.disgusted * 100).toFixed(2); 
    document.getElementById('userSuprised').innerHTML = (finalResult.surprised * 100).toFixed(2); 
};
let rawData = []
let finalResultHundredths = []

startFaceApiDetection = function () {
    console.log('Starting face api detection')
    const facialRecognitionContainer = document.getElementById('facial-recognition')
    const canvas = faceapi.createCanvasFromMedia(video)
    facialRecognitionContainer.insertBefore(canvas, facialRecognitionContainer.firstChild)
    const displaySize = {width: video.width, height: video.height}
    faceapi.matchDimensions(canvas, displaySize)
    const interval = setInterval(async () => {
        if (videoJsPlayer.currentTime() === videoJsPlayer.duration() ) {
            faceDetectionStopped = true
            stopFaceDetection(video.srcObject)
            clearInterval(interval)
        }
        const currentTime = moment.utc(moment.duration(videoJsPlayer.currentTime(), "seconds").asMilliseconds()).format("mm:ss")
        const videoName = getVideoName(videoJsPlayer.currentSrc())
        const detections = await faceapi.detectAllFaces(video, new faceapi.TinyFaceDetectorOptions())
        .withFaceLandmarks()
        .withFaceExpressions()
        const resizedDetections = faceapi.resizeResults(detections, displaySize)
        
        console.log(detections)
        //getting the detection length rather than expression length
        if (detections.length) {
            const expressions = detections.map(detection => ({...detection.expressions}))
                .reduce((acc, cur) => {
                    for (let i in cur) {
                        if (cur.hasOwnProperty(i)) {
                            acc[i] = (acc[i] || 0) + cur[i]
                        }
                    }
                    return acc
                }, {})
            const expressionValueHolder = {}
            config.data.labels.push(currentTime);
            config.data.datasets.forEach(dataset => {
                const averageSentiments = expressions[dataset.label.toLowerCase()] / detections.length
                dataset.data.push(averageSentiments)
                expressionValueHolder[dataset.label.toLowerCase()] = averageSentiments
                chart.update()
            })
            rawData.push({
                time: currentTime,
                ...expressionValueHolder
            })
        } else {
            rawData.push({
                time: currentTime,
                angry: 0,
                disgusted: 0,
                fearful: 0,
                happy: 0,
                neutral: 0,
                sad: 0,
                surprised: 0
            })
        }
        canvas.getContext('2d').clearRect(0, 0, canvas.width, canvas.height)
        faceapi.draw.drawDetections(canvas, resizedDetections) //draw box for detected face
        faceapi.draw.drawFaceExpressions(canvas, resizedDetections) // indicate face expression

        if (faceDetectionStopped) {
            config.data.datasets.forEach(dataset => {
                expressionLabels.forEach(expression => {
                    if (dataset.label.toLowerCase() === expression) {
                        finalResult[expression] = dataset.data.length > 0 ? (getSumOfNumbers(dataset.data) / dataset.data.length) : 0
                    }
                    
                })
            })
            
            let storage = {}
            if (localStorage.getItem("rawData")) {
                storage = JSON.parse(localStorage.getItem("rawData"))
            }
            storage = {
                ...storage,
                [videoName]: [...rawData]
            }
            localStorage.setItem(`rawData`, JSON.stringify(storage))

            finalResultHundredths = getResultConvertedToHundredths(rawData)
            dataResultTable();
            resultButton.disabled = false;
        }
    }, 1000)
}

// video.addEventListener('play', startFaceApiDetection)


function exportTableToExcel(tableName, filename){
    var downloadLink;
    var dataType = 'application/vnd.ms-excel';
    var tableSelect = document.getElementById(tableName);
    var dateNow = (new Date().getMonth() + 1) + "-" + new Date().getDate() + "-" + new Date().getFullYear()
    var tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');
    
    
    filename = filename?filename+` ${dateNow}`+'.xls':'excel_data.xls';
    downloadLink = document.createElement("a");
    document.body.appendChild(downloadLink);
    if(navigator.msSaveOrOpenBlob){
        var blob = new Blob(['\ufeff', tableHTML], {
            type: dataType
        });
        navigator.msSaveOrOpenBlob( blob, filename);
    }else{
        downloadLink.href = 'data:' + dataType + ', ' + tableHTML;
        downloadLink.download = filename;
        downloadLink.click();
    }
};

function convertToCSV(objArray) {
    var array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
    var str = '';

    for (var i = 0; i < array.length; i++) {
        var line = '';
        for (var index in array[i]) {
            if (line != '') line += ','

            line += array[i][index];
        }

        str += line + '\r\n';
    }

    return str;
}

function exportCSVFile(headers, items, fileTitle) {
    if (headers) {
        items.unshift(headers);
    }

    // Convert Object to JSON
    var jsonObject = JSON.stringify(items);

    var csv = this.convertToCSV(jsonObject);

    var exportedFilenmae = fileTitle + '.csv' || 'export.csv';

    var blob = new Blob([csv], { type: 'text/csv;charset=utf-8;' });
    if (navigator.msSaveBlob) { // IE 10+
        navigator.msSaveBlob(blob, exportedFilenmae);
    } else {
        var link = document.createElement("a");
        if (link.download !== undefined) { // feature detection
            // Browsers that support HTML5 download attribute
            var url = URL.createObjectURL(blob);
            link.setAttribute("href", url);
            link.setAttribute("download", exportedFilenmae);
            link.style.visibility = 'hidden';
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
        }
    }
}

function download(){
  var headers = {
    time: "Time",
    neutral: "Neutral",
    happy: "Happy",
    sad: "Sad",
    angry: "Angry",
    fearful: "Fearful",
    disgusted: "Disgusted",
    surprised: "Surprised"
  };

  var fileTitle = 'Detailed Data Summary';

  exportCSVFile(headers, finalResultHundredths, fileTitle); // call the exportCSVFile() function to process the JSON and trigger the download
}

function rangeSlide(id, value) {
    document.getElementById(id).innerHTML = value
}