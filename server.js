var express = require('express');
var path = require('path');
const cors = require('cors')
const multer = require('multer')
const fs = require('fs')

var app = express();

const PRESENTATIONS_DIRNAME = '/presentations'

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './presentations')
    },
    filename: function (req, file, cb) {
        cb(null, file.originalname)
    }
})

const uploadDisk = multer({storage: storage})

app.use(express.static(__dirname));

app.use(cors())
app.use('/static', express.static('presentations'))

// API exposed endpoints

app.get('/api/presentations', function(req, res) {
    res.setHeader('Content-Type', 'application/json')

    const files = fs.readdirSync(path.join(__dirname, PRESENTATIONS_DIRNAME))
    res.json(files)
})

app.post('/api/file-upload', uploadDisk.single('presentation-file'), (req, res) => {
    console.log('Presentation upload has been reached')
    res.send('Uploaded successfully')
})

app.listen(8001, function() {
  console.log('Running on PORT 8001');
});
