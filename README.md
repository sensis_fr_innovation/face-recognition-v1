# Sense Sees
Pre-requisites:  
1. Node 10 or higher
2. NPM 6 or higher
   
How to run:  
1. Open terminal
2. Go to face-recognition-v1 directory
3. run 'npm install'
4. run 'npm start'
5. Open browser then go to localhost:8001
   
How to use "Sense Sees" :
1. Make sure the machine allows the application to access webcam.
2. Choose video file to upload
3. After uploading, select the video from the list ('Choose a presentation')
4. Click "Start Presentation" under Preview section
5. Wait for the video presentation to finish
6. To view Data_Summary of emotions, click "Data Summary" button
7. To generate a report file, click on "Export Data Summary" , it will download a .csv file containing the data on the table
8. To generate a more detailed file, click on "Export Detailed Data Summary", it will download a .csv file containing emotion recorded per second.  
   