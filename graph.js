var ctx = document.getElementById('myChart').getContext('2d');
const chartColors = {
  red: 'rgb(255, 99, 132)',
  orange: 'rgb(255, 159, 64)',
  yellow: 'rgb(255, 205, 86)',
  green: 'rgb(75, 192, 192)',
  blue: 'rgb(54, 162, 235)',
  purple: 'rgb(153, 102, 255)',
  grey: 'rgb(201, 203, 207)'
};

function onRefresh(chart) {
  chart.config.data.datasets.forEach(function(dataset) {
    dataset.data.push({
      x: Date.now(),
      y: 0
    });
  });
}

var color = Chart.helpers.color;
var config = {
  type: 'line',
  data: {
    labels: ["00:00"],
    datasets: [
      {
      label: 'neutral',
      backgroundColor: color(chartColors.grey).alpha(0.5).rgbString(),
      borderColor: chartColors.grey,
      fill: false,
      cubicInterpolationMode: 'monotone',
      data: []
      },
      {
      label: 'happy',
      backgroundColor: color(chartColors.green).alpha(0.5).rgbString(),
      borderColor: chartColors.green,
      fill: false,
      cubicInterpolationMode: 'monotone',
      data: []
      },
      {
      label: 'sad',
      backgroundColor: color(chartColors.orange).alpha(0.5).rgbString(),
      borderColor: chartColors.orange,
      fill: false,
      cubicInterpolationMode: 'monotone',
      data: []
      },
      {
      label: 'angry',
      backgroundColor: color(chartColors.red).alpha(0.5).rgbString(),
      borderColor: chartColors.red,
      fill: false,
      cubicInterpolationMode: 'monotone',
      data: []
      },
      {
      label: 'fearful',
      backgroundColor: color(chartColors.purple).alpha(0.5).rgbString(),
      borderColor: chartColors.purple,
      fill: false,
      cubicInterpolationMode: 'monotone',
      data: []
      },
      {
      label: 'disgusted',
      backgroundColor: color(chartColors.yellow).alpha(0.5).rgbString(),
      borderColor: chartColors.yellow,
      fill: false,
      cubicInterpolationMode: 'monotone',
      data: []
      },
      {
      label: 'surprised',
      backgroundColor: color(chartColors.blue).alpha(0.5).rgbString(),
      borderColor: chartColors.blue,
      fill: false,
      cubicInterpolationMode: 'monotone',
      data: []
      }
    ]
  },
  options: {
    responsive: true,
    maintainAspectRatio: false,
    title: {
      display: true,
      text: 'Average Sentiments'
    },
    scales: {
      xAxes: [{
        display: true,
        scaleLabel: {
          display: true,
          labelString: 'Timeframe'
        }
      }],
      yAxes: [{
        display: true,
        scaleLabel: {
          display: true,
          labelString: 'Sentiment'
        }
      }]
    },
    tooltips: {
      mode: 'nearest',
      intersect: false
    },
    hover: {
      mode: 'nearest',
      intersect: false
    }
  }
};


var chart = new Chart(ctx, config);
